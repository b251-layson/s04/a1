from abc import ABC, abstractclassmethod

class Animal(ABC):
    def eat(self, food):
        self._food = food
        print(f"Eaten {food}")

    def make_sound(self):
        pass


class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def call(self):
        print(f"{self._name}, come on!")

    def make_sound(self):
        print("Miaow! Nyaw! nyaaaaa!")

    # setter methods
    def set_name(self, name):
        self._name = name

    # getter methods
    def get_name(self):
        print(f"{self._name}")

    def get_breed(self):
        print(f"{self._breed}")

    def get_age(self):
        print(f"{self._age}")


class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def call(self):
        print(f"Here {self._name}")

    def make_sound(self):
        print("Bark! Woof! Arf!")

    # setter methods
    def set_name(self, name):
        self._name = name

    # getter methods
    def get_name(self):
        print(f"{self._name}")

    def get_breed(self):
        print(f"{self._breed}")

    def get_age(self):
        print(f"{self._age}")


dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()